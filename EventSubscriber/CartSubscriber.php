<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 13:24
 */

namespace eezeecommerce\DiscountBundle\EventSubscriber;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use eezeecommerce\CartBundle\CartEvents;
use eezeecommerce\CartBundle\Event\FilterItemsEvent;
use eezeecommerce\DiscountBundle\Calculator\DiscountCalculator;
use eezeecommerce\DiscountBundle\Entity\DiscountAmounts;
use eezeecommerce\DiscountBundle\Entity\DiscountCodes;
use eezeecommerce\DiscountBundle\Entity\Discounts;
use eezeecommerce\DiscountBundle\Provider\DiscountProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CartSubscriber implements EventSubscriberInterface
{

    protected $discounts;

    protected $type = "percentage";

    public function __construct(DiscountProvider $provider)
    {
        $this->discounts = $provider->loadDiscounts();
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            CartEvents::CART_SAVE_INITIALISE => array("postSaveDiscount", 10),
            CartEvents::CART_UPDATE_PRICING_INITIALISED => array("postSaveDiscount", 10),
        );
    }

    public function postSaveDiscount(FilterItemsEvent $event)
    {
        $this->getSubTotal($items = $event->getItems());

        $discountFigure = 0;

        if (null === $this->discounts) {
            return;
        }

        if (($discount = $this->discounts) instanceof Discounts)
        {
            $this->type = $this->discounts->getType();
            $discountFigure = $this->checkDiscount($this->total, $this->discounts->getDiscountAmount());
        } elseif (is_array($this->discounts)) {
            foreach($this->discounts as $d)
            {
                if (!$d instanceof Discounts) {
                    continue;
                }
                $this->type = $d->getType();
                $discountFigure = $this->checkDiscount($this->total, $d->getDiscountAmount());
            }
        } elseif ($this->discounts instanceof DiscountCodes)
        {
            $this->type = "percentage";
            $discountFigure = $this->discounts->getPercentageOff();
        }

        $discount = new DiscountCalculator();
        $discount->setDiscount($discountFigure);
        $discount->setDiscountType($this->type);

        foreach ($items as $item) {
            $item->setDiscount($discount->getDiscountTotal($item->getSubTotal()));
        }
    }

    private function checkDiscount($total, PersistentCollection $discounts)
    {

        foreach ($discounts as $discount)
        {
            if ($total >= $discount->getTotal()) {
                return $discount->getDiscountAmount();
            }
        }
        return;
    }

    private function getSubTotal(array $items)
    {
        $total = array();

        foreach ($items as $item) {
            array_push($total, $item->getSubtotal());
        }

        $this->total = array_sum($total);
    }
}