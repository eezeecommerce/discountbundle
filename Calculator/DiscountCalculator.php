<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 11:23
 */

namespace eezeecommerce\DiscountBundle\Calculator;


class DiscountCalculator extends AbstractCalculator
{

    /**
     * Returns Discount amount as an float
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }



    /**
     * Returns Discount total base on amount input
     *
     * @param float $subtotal Subtotal of order
     *
     * @return float
     */
    public function getDiscountTotal($subtotal)
    {
        return parent::getDiscountTotal($subtotal);
    }
}