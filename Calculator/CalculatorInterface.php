<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 11:18
 */

namespace eezeecommerce\DiscountBundle\Calculator;


interface CalculatorInterface
{
    /**
     * Returns Discount amount as an float
     *
     * @return float
     */
    public function getDiscount();

    /**
     * Set Discount amount
     *
     * @param float $discount Discount amount
     *
     * @return float
     */
    public function setDiscount($discount);

    /**
     * Sets type of discount
     *
     * @param enum("fixed", "percentage") $type Set the type of discount
     *
     * @return void
     */
    public function setDiscountType($type);

    /**
     * Returns Discount total base on amount input
     *
     * @param float $subtotal Subtotal of order
     *
     * @return float
     */
    public function getDiscountTotal($subtotal);

}