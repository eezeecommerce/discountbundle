<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 11:35
 */

namespace eezeecommerce\DiscountBundle\Calculator;

use eezeecommerce\DiscountBundle\Calculator\CalculatorInterface;

abstract class AbstractCalculator implements CalculatorInterface
{
    /**
     * @var string
     */
    protected $type = null;

    /**
     * @var float
     */
    protected $discount = 0.00;

    /**
     * Sets type of discount
     *
     * @param enum("fixed", "percentage") $type Set the type of discount
     *
     * @return void
     */
    public function setDiscountType($type)
    {
        if (!is_string($type)) {
            throw new \InvalidArgumentException(sprintf("Type should be a string. Instead %s was used", gettype($type)));
        }

        $type = strtolower($type);

        $array = array("fixed", "percentage");
 
        if (!in_array($type, $array)) {
            throw new \InvalidArgumentException(sprintf("Type should be either fixed or percentage. Instead %s was used", $type));
        }

        $this->type = $type;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $discount = (float) $discount;

        $this->discount = $discount;
    }

    public function getDiscountTotal($subtotal)
    {
        if (null === $this->type) {
            throw new \InvalidArgumentException(sprintf("Type should be set to either Type or Percentage. Instead type is set to %s", gettype($this->type)));
        }

        if ($this->discount === 0.00) {
            return 0.00;
        }

        if ($this->type == "fixed") {
            return round(
                $subtotal - ($subtotal - $this->discount), 2, PHP_ROUND_HALF_DOWN
            );
        }

        $decimal = 1 - ($this->discount / 100);

        return round(
            $subtotal - ($subtotal * $decimal), 2, PHP_ROUND_HALF_DOWN
        );
    }

}