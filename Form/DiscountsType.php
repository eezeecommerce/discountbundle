<?php

namespace eezeecommerce\DiscountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiscountsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => false
            ))
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    "percentage" => "percentage off (e.g. 5% off)",
                    "fixed" => "fixed amount off (e.g. £5 off)"
                ),
                'label' => false
            ))
            ->add('action', ChoiceType::class, array(
                'choices' => array("cart" => "Cart"),
                'label' => false
            ))
            ->add('discount_method', "entity", array(
                'class' => "eezeecommerceDiscountBundle:DiscountMethods",
                'property' => "type",
                'label' => false
            ))
            ->add('discount_amount', "collection", array(
                "label" => false,
                'type' => new DiscountAmountsType(),
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                "by_reference" => false,
                'prototype_name' => "__discount_amounts__"
            ))
            ->add("save", "submit")
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\DiscountBundle\Entity\Discounts'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_discountbundle_discounts';
    }
}
