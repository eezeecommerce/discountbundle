<?php

namespace eezeecommerce\DiscountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiscountAmountsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('total', null, array(
                "label" => "Minimum amount to trigger discount"
            ))
            ->add('discount_amount', null, array(
                "label" => "Percentage/Amount discounted"
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\DiscountBundle\Entity\DiscountAmounts'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_discountbundle_discountamounts';
    }
}
