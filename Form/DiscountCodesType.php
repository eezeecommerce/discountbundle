<?php

namespace eezeecommerce\DiscountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiscountCodesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('start', 'datetime', array(
            ))
            ->add('end', 'datetime', array(
            ))
            ->add('percentage_off')
            ->add("save", "submit")
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\DiscountBundle\Entity\DiscountCodes'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_discountbundle_discountcodes';
    }
}
