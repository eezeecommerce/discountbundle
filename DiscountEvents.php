<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/02/16
 * Time: 12:07
 */

namespace eezeecommerce\DiscountBundle;


final class DiscountEvents
{

    /**
     * The discount.cart event is throw each time an item is added
     * to the cart
     *
     * The event listener receives an
     * eezeecommerce\CartBundle\Event\CartDiscountEvent instance.
     *
     * @var string
     */
    const DISCOUNT_CART = 'discount.cart';
}