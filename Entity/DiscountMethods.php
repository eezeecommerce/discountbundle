<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/02/16
 * Time: 11:15
 */

namespace eezeecommerce\DiscountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="eezeecommerce\DiscountBundle\Entity\DiscountMethodsRepository")
 */
class DiscountMethods
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('customer', 'role', 'global')")
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="Discounts", mappedBy="discount_method")
     */
    private $discount;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->discount = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DiscountMethods
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add discount
     *
     * @param \eezeecommerce\DiscountBundle\Entity\Discounts $discount
     *
     * @return DiscountMethods
     */
    public function addDiscount(\eezeecommerce\DiscountBundle\Entity\Discounts $discount)
    {
        $this->discount[] = $discount;

        return $this;
    }

    /**
     * Remove discount
     *
     * @param \eezeecommerce\DiscountBundle\Entity\Discounts $discount
     */
    public function removeDiscount(\eezeecommerce\DiscountBundle\Entity\Discounts $discount)
    {
        $this->discount->removeElement($discount);
    }

    /**
     * Get discount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
