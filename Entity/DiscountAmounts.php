<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/02/16
 * Time: 10:48
 */

namespace eezeecommerce\DiscountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="eezeecommerce\DiscountBundle\Entity\DiscountAmountsRepository")
 */
class DiscountAmounts
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $total;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $discount_amount;

    /**
     * @ORM\ManyToOne(targetEntity="Discounts", inversedBy="discount_amount")
     * @ORM\JoinColumn(name="discounts_id", referencedColumnName="id")
     */
    protected $discount;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return DiscountAmounts
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set discountAmount
     *
     * @param string $discountAmount
     *
     * @return DiscountAmounts
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discount_amount = $discountAmount;

        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return string
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * Set discount
     *
     * @param \eezeecommerce\DiscountBundle\Entity\Discounts $discount
     *
     * @return DiscountAmounts
     */
    public function setDiscount(\eezeecommerce\DiscountBundle\Entity\Discounts $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return \eezeecommerce\DiscountBundle\Entity\Discounts
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
