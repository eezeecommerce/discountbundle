<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/02/16
 * Time: 10:49
 */

namespace eezeecommerce\DiscountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="eezeecommerce\DiscountBundle\Entity\DiscountsRepository")
 */
class Discounts
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('percentage','fixed')", nullable=false)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('cart')", nullable=false)
     */
    protected $action;

    /**
     * @ORM\ManyToOne(targetEntity="DiscountMethods", inversedBy="discount")
     * @ORM\JoinColumn(name="discount_method", referencedColumnName="id")
     */
    private $discount_method;

    /**
     * @ORM\OneToMany(targetEntity="\eezeecommerce\UserBundle\Entity\User", mappedBy="discount")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="DiscountAmounts", mappedBy="discount", cascade={"persist"})
     * @ORM\OrderBy({"total" = "DESC"})
     */
    private $discount_amount;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->discount_amount = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Discounts
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Discounts
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Discounts
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set discountMethod
     *
     * @param \eezeecommerce\DiscountBundle\Entity\DiscountMethods $discountMethod
     *
     * @return Discounts
     */
    public function setDiscountMethod(\eezeecommerce\DiscountBundle\Entity\DiscountMethods $discountMethod = null)
    {
        $this->discount_method = $discountMethod;

        return $this;
    }

    /**
     * Get discountMethod
     *
     * @return \eezeecommerce\DiscountBundle\Entity\DiscountMethods
     */
    public function getDiscountMethod()
    {
        return $this->discount_method;
    }

    /**
     * Add discountAmount
     *
     * @param \eezeecommerce\DiscountBundle\Entity\DiscountAmounts $discountAmount
     *
     * @return Discounts
     */
    public function addDiscountAmount(\eezeecommerce\DiscountBundle\Entity\DiscountAmounts $discountAmount)
    {
        $this->discount_amount[] = $discountAmount;

        $discountAmount->setDiscount($this);

        return $this;
    }

    /**
     * Remove discountAmount
     *
     * @param \eezeecommerce\DiscountBundle\Entity\DiscountAmounts $discountAmount
     */
    public function removeDiscountAmount(\eezeecommerce\DiscountBundle\Entity\DiscountAmounts $discountAmount)
    {
        $this->discount_amount->removeElement($discountAmount);
    }

    /**
     * Get discountAmount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Add user
     *
     * @param \eezeecommerce\UserBundle\Entity\User $user
     *
     * @return Discounts
     */
    public function addUser(\eezeecommerce\UserBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \eezeecommerce\UserBundle\Entity\User $user
     */
    public function removeUser(\eezeecommerce\UserBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
