<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/03/16
 * Time: 10:17
 */

namespace eezeecommerce\DiscountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use eezeecommerce\DiscountBundle\Validator\Constraints as EezeeAssert;

/**
 * @ORM\Entity
 * @EezeeAssert\CheckTime
 */
class DiscountCodes
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $code;

    /**
     * @ORM\Column(type="integer")
     */
    protected $percentage_off;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    protected $start;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    protected $end;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return DiscountCodes
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return DiscountCodes
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return DiscountCodes
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set percentageOff
     *
     * @param integer $percentageOff
     *
     * @return DiscountCodes
     */
    public function setPercentageOff($percentageOff)
    {
        $this->percentage_off = $percentageOff;

        return $this;
    }

    /**
     * Get percentageOff
     *
     * @return integer
     */
    public function getPercentageOff()
    {
        return $this->percentage_off;
    }
}
