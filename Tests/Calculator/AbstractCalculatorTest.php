<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 11:35
 */

namespace eezeecommerce\DiscountBundle\Tests\Calculator;

use eezeecommerce\DiscountBundle\Calculator\AbstractCalculator;
use eezeecommerce\DiscountBundle\Calculator\CalculatorInterface;


class AbstractCalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testAbstractCalculatorImplementsInterface()
    {
        $stub = new \ReflectionClass(AbstractCalculator::class);

        $this->assertTrue($stub->implementsInterface(CalculatorInterface::class));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Type should be either fixed or percentage. Instead asd was used
     */
    public function testTypeCanOnlyBeFixedOrPercentage()
    {
        $stub = $this->getMockForAbstractClass(AbstractCalculator::class);

        $class = new \ReflectionClass($stub);

        $method = $class->getMethod("setDiscountType");

        $method->invoke($stub, "fixed");

        $method->invoke($stub, "percentage");

        $method->invoke($stub, "asd");
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Type should be a string. Instead object was used
     */
    public function testTypeThrowsErrorIfNonStringIsPassedThrough()
    {
        $fakeMock = $this->getMockBuilder("stdClass")
        ->getMock();

        $stub = $this->getMockForAbstractClass(AbstractCalculator::class);

        $class = new \ReflectionClass($stub);

        $method = $class->getMethod("setDiscountType");

        $method->invoke($stub, $fakeMock);
    }
}