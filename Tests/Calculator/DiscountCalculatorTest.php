<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 11:23
 */

namespace eezeecommerce\DiscountBundle\Tests\Calculator;

use eezeecommerce\DiscountBundle\Calculator\DiscountCalculator;
use eezeecommerce\DiscountBundle\Calculator\AbstractCalculator;


class DiscountCalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testGlobalDiscountImplementsCalculatorInterface()
    {
        $stub = new \ReflectionClass(DiscountCalculator::class);

        $this->assertTrue($stub->isSubclassOf(AbstractCalculator::class));
    }

    public function testFixedDiscountReturnsValidResult()
    {
        $subtotal = 90;

        $calc = new DiscountCalculator();
        $calc->setDiscountType("fixed");
        $calc->setDiscount(15);

        $this->assertEquals(15.00, $calc->getDiscount());

        $this->assertEquals(15, $calc->getDiscountTotal($subtotal));
    }

    public function testPercentageDiscountRoundsDownOnDecimal()
    {
        $calc = new DiscountCalculator();
        $calc->setDiscountType("percentage");
        $calc->setDiscount(50);

        $this->assertEquals(0.37, $calc->getDiscountTotal(0.75));


        $this->assertEquals(8.97, $calc->getDiscountTotal(17.95));
    }

    public function testPercentageAmountReturnsValidAmount()
    {
        $subtotal = 90;

        $calc = new DiscountCalculator();
        $calc->setDiscountType("percentage");
        $calc->setDiscount(20);

        $this->assertEquals(20.00, $calc->getDiscount());

        $this->assertEquals(18, $calc->getDiscountTotal($subtotal));
    }

    public function testDiscountTotalReturnsZeroIfAmountNotSet()
    {
        $subtotal = 90;

        $calc = new DiscountCalculator();
        $calc->setDiscountType("percentage");

        $this->assertEquals(0.00, $calc->getDiscount());

        $this->assertEquals(0.00, $calc->getDiscountTotal($subtotal));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Type should be set to either Type or Percentage. Instead type is set to NULL
     */
    public function testDiscountThrowsErrorIfTypeNotSet()
    {
        $subtotal = 90;

        $calc = new DiscountCalculator();

        $calc->getDiscountTotal($subtotal);
    }

}