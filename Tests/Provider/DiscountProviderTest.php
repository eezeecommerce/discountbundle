<?php

namespace eezeecommerce\DiscountBundle\Tests\Provider;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use eezeecommerce\DiscountBundle\Entity\Discounts;
use eezeecommerce\DiscountBundle\Provider\DiscountProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use eezeecommerce\DiscountBundle\Entity\DiscountCodes;
use Doctrine\ORM\EntityRepository;
use eezeecommerce\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use eezeecommerce\DiscountBundle\Entity\DiscountsRepository;

class DiscountProviderTest extends \PHPUnit_Framework_TestCase
{

    public function testCodeThatHasExpiredReturnsFalse()
    {

        $tokenInterface = $this->getMockBuilder(TokenInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenInterface->expects($this->any())
            ->method("getUser")
            ->will($this->returnValue(null));

        $token = $this->getMockBuilder(TokenStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $token->expects($this->any())
            ->method("getToken")
            ->will($this->returnValue($tokenInterface));

        $start = new \DateTime();
        $start->sub(new \DateInterval("PT10H30S"));

        $entity = $this->getMockBuilder(DiscountCodes::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity->expects($this->any())
            ->method("getCode")
            ->will($this->returnValue("A"));

        $entity->expects($this->any())
            ->method("getStart")
            ->will($this->returnValue($start));

        $entity->expects($this->any())
            ->method("getEnd")
            ->will($this->returnValue($start));

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->any())
            ->method("findOneBy")
            ->will($this->returnValue($entity));

        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $em->expects($this->once())
            ->method("getRepository")
            ->will($this->returnValue($repository));

        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->any())
            ->method("has")
            ->will($this->returnValue(true));

        $request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack->expects($this->any())
            ->method("getMasterRequest")
            ->will($this->returnValue($request));

        $provider = new DiscountProvider($em, $requestStack, $session, $token);

        $discount = $provider->loadDiscounts();

        $this->assertNotTrue($discount);
    }

    public function testCodeThatHasValidDateReturnsEntity()
    {
        $tokenInterface = $this->getMockBuilder(TokenInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenInterface->expects($this->any())
            ->method("getUser")
            ->will($this->returnValue(null));

        $token = $this->getMockBuilder(TokenStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $token->expects($this->any())
            ->method("getToken")
            ->will($this->returnValue($tokenInterface));

        $start = new \DateTime();

        $entity = $this->getMockBuilder(DiscountCodes::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity->expects($this->any())
            ->method("getCode")
            ->will($this->returnValue("A"));

        $entity->expects($this->any())
            ->method("getStart")
            ->will($this->returnValue($start));

        $entity->expects($this->any())
            ->method("getEnd")
            ->will($this->returnValue($start));

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->any())
            ->method("findOneBy")
            ->will($this->returnValue($entity));

        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $em->expects($this->once())
            ->method("getRepository")
            ->will($this->returnValue($repository));

        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->any())
            ->method("has")
            ->will($this->returnValue(true));

        $request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack->expects($this->any())
            ->method("getMasterRequest")
            ->will($this->returnValue($request));

        $provider = new DiscountProvider($em, $requestStack, $session, $token);

        $discount = $provider->loadDiscounts();

        $this->assertEquals($entity, $discount);
    }

    public function testNoCodeReturnsDiscountEntity()
    {
        $tokenInterface = $this->getMockBuilder(TokenInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenInterface->expects($this->once())
            ->method("getUser")
            ->will($this->returnValue(null));

        $token = $this->getMockBuilder(TokenStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $token->expects($this->any())
            ->method("getToken")
            ->will($this->returnValue($tokenInterface));

        $entity = $this->getMockBuilder(Discounts::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity->expects($this->any())
            ->method("getAction")
            ->will($this->returnValue("cart"));

        $entity1 = $this->getMockBuilder(Discounts::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity1->expects($this->any())
            ->method("getAction")
            ->will($this->returnValue("cart"));

        $arrayCollection = new ArrayCollection();
        $arrayCollection->add($entity);
        $arrayCollection->add($entity1);

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array("findByMethod"))
            ->getMock();

        $repository->expects($this->any())
            ->method("findByMethod")
            ->will($this->returnValue($arrayCollection));

        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $em->expects($this->once())
            ->method("getRepository")
            ->will($this->returnValue($repository));

        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->any())
            ->method("has")
            ->will($this->returnValue(false));

        $request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack->expects($this->any())
            ->method("getMasterRequest")
            ->will($this->returnValue($request));

        $provider = new DiscountProvider($em, $requestStack, $session, $token);

        $discount = $provider->loadDiscounts();

        $this->assertEquals($arrayCollection, $discount);
    }

    public function testUserWithDiscountReturnsDiscountEntity()
    {
        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity = $this->getMockBuilder(Discounts::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity->expects($this->any())
            ->method("getAction")
            ->will($this->returnValue("cart"));

        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->any())
            ->method("has")
            ->will($this->returnValue(false));

        $user = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();

        $user->expects($this->once())
            ->method("getDiscount")
            ->will($this->returnValue($entity));

        $tokenInterface = $this->getMockBuilder(TokenInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenInterface->expects($this->once())
            ->method("getUser")
            ->will($this->returnValue($user));

        $token = $this->getMockBuilder(TokenStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $token->expects($this->any())
            ->method("getToken")
            ->will($this->returnValue($tokenInterface));


        $request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack->expects($this->any())
            ->method("getMasterRequest")
            ->will($this->returnValue($request));

        $provider = new DiscountProvider($em, $requestStack, $session, $token);

        $discount = $provider->loadDiscounts();

        $this->assertEquals($entity, $discount);
    }
}