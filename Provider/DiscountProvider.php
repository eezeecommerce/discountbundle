<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 15:09
 */

namespace eezeecommerce\DiscountBundle\Provider;


use Doctrine\ORM\EntityManager;
use eezeecommerce\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Role\Role;

class DiscountProvider
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var User
     */
    protected $user;

    public function __construct(EntityManager $em, RequestStack $request, SessionInterface $session, TokenStorage $token)
    {
        $this->em = $em;
        $this->request = $request->getMasterRequest();
        $this->session = $session;
        $this->user = $token;
        $token->getToken();
    }

    /**
     * @return array|\eezeecommerce\DiscountBundle\Entity\DiscountCodes|\eezeecommerce\DiscountBundle\Entity\Discounts|void
     */
    public function loadDiscounts()
    {
        if ($this->session->has("discount_code")) {

            $code = $this->em->getRepository("eezeecommerceDiscountBundle:DiscountCodes")
                ->findOneBy(array("code" => $this->session->get("discount_code")));

            $now = new \DateTime();

            if ($code->getStart() <= $now && $code->getEnd() >= $now) {
                return $code;
            }

            return;
        }
        if (null !== ($this->user = $this->user->getToken())) {
            if (($user = $this->user->getUser()) instanceof User) {
                if (!empty(($discount = $user->getDiscount()))) {
                    return $discount;
                }
            }
        }

        return $this->em->getRepository("eezeecommerceDiscountBundle:Discounts")
            ->findByMethod("global");
    }
}