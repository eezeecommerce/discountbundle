<?php

namespace eezeecommerce\DiscountBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckTimeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value->getStart() >= $value->getEnd()) {
            $this->context->addViolation(
                $constraint->message,
                array()
            );
        }
    }
}