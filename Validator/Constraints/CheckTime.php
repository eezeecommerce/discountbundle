<?php

namespace eezeecommerce\DiscountBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CheckTime extends Constraint
{
    public $message = "End date must be greater than start time";

    public function validatedBy()
    {
        return 'eezeecommerce\DiscountBundle\Validator\Constraints\CheckTimeValidator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}